/**
 * 2018-12-19: Michael: Trying tutorial from
 * - http://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver/
 * - http://www.sqlitetutorial.net/sqlite-java/
 **/

package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class InsertApp extends ConnectionSQLite {

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextField txtQuantity;
    @FXML
    private TextField txtCost;
    @FXML
    private TextField txtSaleprice;

    @FXML
    public void insertRecord(ActionEvent event) {
        insertItem(
                this.txtName.getText(),
                this.txtDescription.getText(),
                Double.parseDouble(txtQuantity.getText()),
                Double.parseDouble(this.txtCost.getText()),
                Double.parseDouble(this.txtSaleprice.getText()));
    }

    /**
     * Insert a new row into the Item table
     *
     * @param name
     * @param description
     * @param quantity
     * @param cost
     * @param saleprice
     **/
    @FXML
    public void insertItem(String name, String description, double quantity, double cost, double saleprice) {
        /*
        String sql = "INSERT INTO Item (name,description,quantity,cost,saleprice) VALUES(?,?,?,?,?)";

        //Connection conn = dbConnection.getConnection();
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, description);
            pstmt.setDouble(3, quantity);
            pstmt.setDouble(4, cost);
            pstmt.setDouble(5, saleprice);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        */
        String sql = "INSERT INTO Item (name,description,quantity,cost,saleprice) VALUES(?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, description);
            pstmt.setDouble(3, quantity);
            pstmt.setDouble(4, cost);
            pstmt.setDouble(5, saleprice);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }



    /**
     * Insert a new row into the Transaction table
     *
     * @param parent_id
     * @param date
     * @param item_id
     * @param quantity
     * @param transaction_type_id
     * @param amount
     **/

    @FXML
    public void insertTransaction(int parent_id, String date, int item_id, double quantity, int transaction_type_id, double amount) {
        String sql = "INSERT INTO Transaction (parent_id, date, item_id, quantity, transaction_type_id, amount) VALUES(?,?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, parent_id);
            pstmt.setString(2, date);
            pstmt.setInt(3, item_id);
            pstmt.setDouble(4, quantity);
            pstmt.setInt(5, transaction_type_id);
            pstmt.setDouble(5, amount);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    @FXML
    public static void main(String[] args) {

        InsertApp app = new InsertApp();
        // insert new rows
        // app.insertItem("Netgear WG311T Wifi Adapter", "Netgear WG311T Wifi Adapter 108 Mbps 32-bit PCI", 15, 11.00, 15.00);
        //app.insertItem("Kingston KVR400X64C3A", "Kingston DDR3 RAM", 50, 50.00, 75.00);


    }

}
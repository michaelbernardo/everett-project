package com.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateApp extends ConnectionSQLite {

    /**
     * Connect to the database
     *
     * @return the Connection object
     */

    /*
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C:\\Users\\mbernardo.CORPCONT\\sqlitedbs\\everett.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    */

    /**
     * Update data of a Item specified by the id
     *
     * @param id
     * @param name
     * @param description
     * @param quantity
     * @param cost
     * @param saleprice
     */
    public void updateItem(int id, String name, String description, double quantity, double cost, double saleprice) {
        String sql = "UPDATE Item SET name = ? , "
                + "description = ?, "
                + "quantity = ?, "
                + "cost = ?, "
                + "saleprice = ? "
                + "WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, name);
            pstmt.setString(2, description);
            pstmt.setDouble(3, quantity);
            pstmt.setDouble(4, cost);
            pstmt.setDouble(5, saleprice);
            pstmt.setInt(6, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Update data of a Transaction specified by the id
     *
     * @param id
     * @param parent_id
     * @param date
     * @param item_id
     * @param quantity
     * @param transaction_type_id
     * @param amount
     */
    public void updateTransaction(int id, int parent_id, String date, int item_id, double quantity, int transaction_type_id, double amount) {
        String sql = "UPDATE Transaction SET parent_id = ? , "
                + "date = ?, "
                + "item_id = ?, "
                + "quantity = ?, "
                + "transaction_type_id = ?, "
                + "amount = ?, "
                + "WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, parent_id);
            pstmt.setString(2, date);
            pstmt.setInt(3, item_id);
            pstmt.setDouble(4, quantity);
            pstmt.setInt(5, transaction_type_id);
            pstmt.setDouble(6, amount);
            pstmt.setInt(7, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // UpdateApp app = new UpdateApp();
        // update the warehouse with id 3
        //app.update(3, "Finished Products", 5500);
        //app.updateItem(7, "Kiiingston KVVR","Update Me!!!", 51, 60, 80);
    }

}

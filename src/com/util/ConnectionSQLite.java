/**
 * 2018-12-19: Michael: Trying tutorial from
 * - http://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver/
 * - http://www.sqlitetutorial.net/sqlite-java/
**/

package com.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSQLite {
    /**
     * Connect to a sample database
     */
    public static void connect_test() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:\\Users\\mbernardo.CORPCONT\\sqlitedbs\\everett.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C:\\Users\\mbernardo.CORPCONT\\sqlitedbs\\everett.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        connect_test();
    }
}
package com.util;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.awt.event.ActionEvent;

public class InsertAppJavaFX extends ConnectionSQLite {

    @FXML
    private void addItem(ActionEvent event)
    {
        String sql = "INSERT INTO `Inventory`(`id`, `Item`, `Description`, `Price`, `Date`) VALUES (? , ?, ?, ?, ?)";
        try
        {
            Connection conn = this.connect();
            PreparedStatement stmt = conn.prepareStatement(sql);
/*
            stmt.setString(1, this.id.getText());
            stmt.setString(2, this.item.getText());
            stmt.setString(3, this.description.getText());
            stmt.setString(4, this.price.getText());
            stmt.setString(5, this.date.getEditor().getText());
*/
            stmt.execute();
            conn.close();

        }
        catch (SQLException e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}

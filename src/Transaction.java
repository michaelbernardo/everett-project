public class Transaction {
    private int id;
    private int parent_id;
    private String date;
    private int item_id;
    private int quantity;
    private int transaction_type_id;
    private float amount;

    public void setId(int id) {
        this.id = id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setTransaction_type_id(int transaction_type_id) {
        this.transaction_type_id = transaction_type_id;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }


}

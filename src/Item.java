public class Item {
    private int id;
    private String name;
    private String description;
    private int quantity;
    private float cost;
    private float saleprice;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setSaleprice(float saleprice) {
        this.saleprice = saleprice;
    }
}

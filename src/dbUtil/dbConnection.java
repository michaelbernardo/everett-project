package dbUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//class to connect to database
public class dbConnection
{
    private static final String conn = "jdbc:sqlite:InventoryDB.sqlite";

    public static Connection getConnection()
            throws SQLException
            //attempting to connect
    {
        try
        {
            Class.forName("org.sqlite.JDBC");

            return DriverManager.getConnection("jdbc:sqlite:InventoryDB.sqlite");
        }
        //catches exception
        catch (ClassNotFoundException|SQLException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}

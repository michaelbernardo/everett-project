package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InventoryData
{
    //setting private string property
    private StringProperty itemID;
    private StringProperty item;
    private StringProperty quantity;
    private StringProperty price;
    private StringProperty cost;

    //constructor
    public InventoryData(String itemID, String item, String quantity, String price, String cost)
    {
        this.itemID = new SimpleStringProperty(itemID);
        this.item = new SimpleStringProperty(item);
        this.quantity = new SimpleStringProperty(quantity);
        this.price = new SimpleStringProperty(price);
        this.cost = new SimpleStringProperty(cost);
    }
//get and Sets throughout
    public String getItemID()
    {
        return (String)this.itemID.get();
    }

    public String getItem()
    {
        return (String)this.item.get();
    }

    public String getQuantity()
    {
        return (String)this.quantity.get();
    }

    public String getPrice()
    {
        return (String)this.price.get();
    }

    public String getCost()
    {
        return (String)this.cost.get();
    }

    public void setItemID(String value)
    {
        this.itemID.set(value);
    }

    public void setItem(String value)
    {
        this.item.set(value);
    }

    public void setQuantity(String value)
    {
        this.quantity.set(value);
    }

    public void setPrice(String value)
    {
        this.price.set(value);
    }

    public void setCost(String value)
    {
        this.cost.set(value);
    }

//public string property that returns x
    public StringProperty itemIDProperty()
    {
        return this.itemID;
    }

    public StringProperty itemProperty()
    {
        return this.item;
    }

    public StringProperty quantityProperty()
    {
        return this.quantity;
    }

    public StringProperty priceProperty()
    {
        return this.price;
    }

    public StringProperty costProperty()
    {
        return this.cost;
    }
}

package sample;

import dbUtil.dbConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Controller
        implements Initializable
{
    //private textfields
    @FXML
    private TextField itemID;
    @FXML
    private TextField item;
    @FXML
    private TextField quantity;
    @FXML
    private TextField price;
    @FXML
    private TextField cost;
    //tableview
    @FXML
    private TableView<InventoryData> inventoryTable;
    @FXML
    private TableColumn<InventoryData, String> idColumn;
    @FXML
    private TableColumn<InventoryData, String> itemColumn;
    @FXML
    private TableColumn<InventoryData, String> quantityColumn;
    @FXML
    private TableColumn<InventoryData, String> priceColumn;
    @FXML
    private TableColumn<InventoryData, String> costColumn;
    @FXML
    private Button loadbutton;
    private ObservableList<InventoryData> data;
    private dbConnection dc;
//constructor
    public void initialize(URL url, ResourceBundle rb)
    {
        this.dc = new dbConnection();
    }

    @FXML
    //loads the inventory data
    private void loadInventoryData(ActionEvent event)
    {
        try
        {
            //connects to database
            Connection conn = dbConnection.getConnection();
            this.data = FXCollections.observableArrayList();
            //sql query to get results from inventory
            ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM Inventory");
            while (rs.next()) {
                this.data.add(new InventoryData(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        }
        //catches exception
        catch (SQLException e)
        {
            System.err.println("Error " + e);
        }
        this.idColumn.setCellValueFactory(new PropertyValueFactory("itemID"));
        this.itemColumn.setCellValueFactory(new PropertyValueFactory("item"));
        this.quantityColumn.setCellValueFactory(new PropertyValueFactory("Quantity"));
        this.priceColumn.setCellValueFactory(new PropertyValueFactory("Price"));
        this.costColumn.setCellValueFactory(new PropertyValueFactory("cost"));

        this.inventoryTable.setItems(null);
        this.inventoryTable.setItems(this.data);
    }

    //adds items to inventory
    @FXML
    private void addItem(ActionEvent event) throws SQLException
    {
        //sql query to insert into database
        String sql = "INSERT INTO `Inventory`(`itemID`, `item`, `quantity`, `price`, `cost`) VALUES (? , ?, ?, ?, ?)";
        try
        {
            //connects to database
            Connection conn = dbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, this.itemID.getText());
            stmt.setString(2, this.item.getText());
            stmt.setString(3, this.quantity.getText());
            stmt.setString(4, this.price.getText());
            stmt.setString(5, this.cost.getText());

            stmt.execute();
            conn.close();
        }
        //catches exception
        catch (SQLException e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    //deletes data from
    @FXML
    private void deleteData(ActionEvent event)
    {
        String sql = "DELETE FROM `Inventory` Where itemID = ?";
        try
        {
            Connection conn = dbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, this.itemID.getText());

            stmt.execute();
            conn.close();
        }
        catch (SQLException e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
    //if u click something on table it moves it to textfield
    @FXML
    private void movetotextfield(){
        inventoryTable.setOnMouseClicked(e -> {
            InventoryData p1 = inventoryTable.getItems().get(inventoryTable.getSelectionModel().getSelectedIndex());
            itemID.setText(p1.getItemID());
            item.setText(p1.getItem());
            quantity.setText(p1.getQuantity());
            price.setText(p1.getPrice());
            cost.setText((p1.getCost()));
        });
    }
    //updates the data by using sql queries
    @FXML
    private void updateData(ActionEvent event)
    {
        String sql = "UPDATE Inventory set item = ?, quantity = ?, price = ?, cost = ? WHERE itemID = ?";
        try
        {
            String id = itemID.getText();
            String name = item.getText();
            String quan = quantity.getText();
            String pricee = price.getText();
            String costt = cost.getText();
            Connection conn = dbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setString(2, quan);
            stmt.setString(3, pricee);
            stmt.setString(4, costt);
            stmt.setString(5,id);

            //lets u know if update went through
            int i = stmt.executeUpdate();
            if(i==1){
                System.out.println("Update confirmed");
            }

            conn.close();
        }
        //catches exception
        catch (SQLException e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
//clears fields of the textfields.
    @FXML
    private void clearFields(ActionEvent event)
    {
        this.itemID.setText("");
        this.item.setText("");
        this.quantity.setText("");
        this.price.setText("");
        this.cost.setText("");
    }
}

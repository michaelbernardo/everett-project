package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

//main method
public class Main extends Application
{

    public void start(Stage stage)
            throws Exception
    {
        //starts the sample page
        Parent root = (Parent)FXMLLoader.load(getClass().getResource("sample.fxml"));
        //new scene is created
        Scene scene = new Scene(root);
        //sets the scene
        stage.setScene(scene);
        stage.setTitle("Everett Inventory System");
        stage.show();
    }
//launches the arguments
    public static void main(String[] args)
    {
        launch(args);
    }
}
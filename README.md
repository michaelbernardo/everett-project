Everett Inventory Management

High Overview
Everett is a simple application software that will help users manage their inventory for any kind of product or service.
Its main features include adding, editing, and deleting items via a Graphical User Interface (GUI). Each item will
include important information including its item name, quantity, price, and cost.

Industry
Industries that will be suitable candidates to use Everett include clothing stores, computer stores, grocery stores, and
furniture stores. Some services can also use Everett such as spas, clinics, and hair cutters. Everett is not intended to
be used in a high traffic environment. Its maximum throughput will be subject to the hardware and software limitations
of computer it will be installed in.

Description
Programming Language: Java
Database: SQLite

Each database table can be edited by a JavaFX GUI. The interface has an add, edit, and delete function to manage its
contents. The following fields can be updated: id, item, quantity, price, cost.

Installation Instructions
1. Clone the project from git@gitlab.com:michaelbernardo/everett-project.git
2. Download the latest SQLite JDBC driver from https://bitbucket.org/xerial/sqlite-jdbc/downloads/ and include it in the
project
3. Build the project and Run.

Listing Items
1. When first opened, the list is empty.
2. Click the ‘Load Data’ button to show all records.

Adding New Items
1. In the left side, click inside the itemID and begin typing the details.
2. Hit tab to move to the next textbox.
3. Click the Add Item button to finish adding a new item.
4. The new item will appear after clicking the Load Data button.

Editing Items
1. Click the Load Data button.
2. Double-click the item to be edited.
3. In the left side, click inside each textbox and begin typing the details.
4. Hit tab to move to the next textbox.
5. Click the Update Data button to finish editing an item.
6. The newly edited item will appear after clicking the Load Data button.

Deleting Items
1. Click the Load Data button.
2. Type in the itemID you want deleted.
3. Click the Delete Data button to finish deleting an item.
4. The row consisting that itemID will be deleted after clicking the Load Data button.


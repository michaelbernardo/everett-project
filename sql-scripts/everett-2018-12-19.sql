--
-- File generated with SQLiteStudio v3.2.1 on Wed Dec 19 12:14:56 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Item
DROP TABLE IF EXISTS Item;
CREATE TABLE Item (id INTEGER PRIMARY KEY, name VARCHAR (250), description VARCHAR (1000), quantity INTEGER, cost DECIMAL, saleprice DECIMAL);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (1, 'Dell Optiplex 3060', 'Intel i5 processor, 8 GB RAM, 128 GB SSD', 10, 900, 1200);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (2, 'Dell Optiplex 3070', 'Intel i5 processor, 16 GB RAM, 128 GB SSD', 10, 1000, 1400);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (3, 'HP EliteDisplay E240', 'HP 24" LED Monitor', 30, 175, 210);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (4, 'LG 24m38D', 'LG 24" LED Monitor', 45, 130, 160);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (5, 'Logitech B120 Mouse', 'Logictech B120 Optical Mouse', 100, 10, 20);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (6, 'Netgear WG311T Wifi Adapter', 'Netgear WG311T Wifi Adapter 108 Mbps 32-bit PCI', 15, 11, 15);
INSERT INTO Item (id, name, description, quantity, cost, saleprice) VALUES (7, 'Kiiingston KVVR', 'Update Me!!!', 51, 60, 80);

-- Table: Manufacturer
DROP TABLE IF EXISTS Manufacturer;
CREATE TABLE "Manufacturer" (
	`ManufacturerID`	TEXT,
	`name`	TEXT,
	`description`	INTEGER,
	`address`	TEXT,
	`City`	TEXT,
	`telephone`	TEXT
);
INSERT INTO Manufacturer (ManufacturerID, name, description, address, City, telephone) VALUES ('1', 'MarkCompany', 'Provides Computers', '1231 Lansdowne St ', 'Peterborough', '705-123-1111');
INSERT INTO Manufacturer (ManufacturerID, name, description, address, City, telephone) VALUES ('2
', 'Jorges', 'Provides Televisons', '411 spillsbury drive', 'Hamilton', '705-556-5667');

-- Table: Transaction
DROP TABLE IF EXISTS "Transaction";
CREATE TABLE "Transaction" (id INTEGER PRIMARY KEY, parent_id INTEGER, date DATETIME, item_id INTEGER, quantity INTEGER, transaction_type_id INTEGER, amount DECIMAL);

-- Table: TransactionsType
DROP TABLE IF EXISTS TransactionsType;
CREATE TABLE "TransactionsType" (
	`1`	TEXT,
	`2`	TEXT,
	`3`	TEXT,
	`4`	TEXT,
	`5`	TEXT,
	`6`	TEXT,
	`7`	TEXT
);

-- Index: 
DROP INDEX IF EXISTS "";
CREATE UNIQUE INDEX "" ON Item (id);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
